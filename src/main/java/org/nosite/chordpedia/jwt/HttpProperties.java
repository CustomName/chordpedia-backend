package org.nosite.chordpedia.jwt;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "http")
public class HttpProperties {

    private String allowOrigin;

    private String allowHeaders;

    private String allowMethods;

}
