package org.nosite.chordpedia.jwt;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.model.AppUser;
import org.nosite.chordpedia.services.AppUserService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final AppUserService appUserService;

    @Override
    public CustomUserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        AppUser appUser = appUserService.findByLogin(userName);

        return CustomUserDetails.fromAppUserToCustomUserDetails(appUser);
    }

}
