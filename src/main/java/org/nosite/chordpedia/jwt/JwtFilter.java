package org.nosite.chordpedia.jwt;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.util.StringUtils.hasText;

@Component
@Slf4j
@AllArgsConstructor
public class JwtFilter extends GenericFilterBean {

    private static final String AUTHORIZATION = "Authorization";
    private static final String ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    private static final String ALLOW_HEADERS = "Access-Control-Allow-Headers";
    private static final String ALLOW_METHODS = "Access-Control-Allow-Methods";

    private final JwtProvider jwtProvider;
    private final CustomUserDetailsService customUserDetailsService;
    private final HttpProperties httpProperties;

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        if(!hasText(((HttpServletResponse) servletResponse).getHeader(ALLOW_ORIGIN))){
            ((HttpServletResponse) servletResponse).addHeader(ALLOW_ORIGIN, httpProperties.getAllowOrigin());
        }

        if(!hasText(((HttpServletResponse) servletResponse).getHeader(ALLOW_HEADERS))){
            ((HttpServletResponse) servletResponse).addHeader(ALLOW_HEADERS, httpProperties.getAllowHeaders());
        }

        if(!hasText(((HttpServletResponse) servletResponse).getHeader(ALLOW_METHODS))){
            ((HttpServletResponse) servletResponse).addHeader(ALLOW_METHODS, httpProperties.getAllowMethods());
        }

        String token = getTokenFromRequest((HttpServletRequest) servletRequest);

        if (token != null && jwtProvider.validateToken(token)) {
            log.info("token valid");
            String login = jwtProvider.getLoginFromToken(token);
            CustomUserDetails customUserDetails = customUserDetailsService.loadUserByUsername(login);
            log.info("getUsername = " + customUserDetails.getUsername());
            log.info("getPassword = " + customUserDetails.getPassword());
            log.info("getAuthorities = " + customUserDetails.getAuthorities());
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(customUserDetails, null, customUserDetails.getAuthorities());
            log.info("auth = " + auth);
            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        log.info("getAuthentication = " + SecurityContextHolder.getContext().getAuthentication());
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private String getTokenFromRequest(HttpServletRequest request) {
        String bearer = request.getHeader(AUTHORIZATION);

        if (hasText(bearer) && bearer.startsWith("Bearer ")) {
            return bearer.substring(7);
        }

        return null;
    }

}