package org.nosite.chordpedia.utils;

import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class Values {

    public static String getUuid(){
        return UUID.randomUUID().toString()
                .replace("-", "")
                .substring(0, 10);
    }

}
