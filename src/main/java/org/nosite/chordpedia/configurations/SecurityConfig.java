package org.nosite.chordpedia.configurations;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.jwt.CustomUserDetailsService;
import org.nosite.chordpedia.jwt.JwtFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilter jwtFilter;
    private final CustomUserDetailsService userDetailsService;

    protected void configure(HttpSecurity http) throws Exception {
        http
                .headers()
                    .frameOptions().disable()
                .and()
                    .csrf().disable()
                    .sessionManagement()
                        .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.OPTIONS, "/api/private/*").permitAll()
                        .antMatchers("/api/private/*").authenticated()
                        .antMatchers("/api/public/*").permitAll()
                .and()
                    .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }

}