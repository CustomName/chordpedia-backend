package org.nosite.chordpedia.repos;

import org.nosite.chordpedia.model.Keynote;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface KeynoteRepository extends CrudRepository<Keynote, Long> {

    Optional<Keynote> findByCode(String code);

}
