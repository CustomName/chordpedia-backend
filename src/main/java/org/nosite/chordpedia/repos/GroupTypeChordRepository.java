package org.nosite.chordpedia.repos;

import org.nosite.chordpedia.model.GroupTypeChord;
import org.springframework.data.repository.CrudRepository;

public interface GroupTypeChordRepository extends CrudRepository<GroupTypeChord, Long> {
}
