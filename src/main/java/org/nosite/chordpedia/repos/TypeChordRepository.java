package org.nosite.chordpedia.repos;

import org.nosite.chordpedia.model.TypeChord;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TypeChordRepository extends CrudRepository<TypeChord, Long> {

    Optional<TypeChord> findByCode(String code);

}
