package org.nosite.chordpedia.repos;

import org.nosite.chordpedia.model.TonalityRules;
import org.springframework.data.repository.CrudRepository;

public interface TonalityRulesRepository extends CrudRepository<TonalityRules, Long> {
}
