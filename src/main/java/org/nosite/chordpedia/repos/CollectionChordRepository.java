package org.nosite.chordpedia.repos;

import org.nosite.chordpedia.model.CollectionChord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CollectionChordRepository extends CrudRepository<CollectionChord, Long> {

    Optional<CollectionChord> findByUuid(String uuid);

    @Query(value = "SELECT * FROM collection_chord WHERE is_admin_author = true", nativeQuery = true)
    List<CollectionChord> findCollectionChordsWithAdminAuthor();

    @Query(value = "SELECT * FROM collection_chord WHERE is_admin_author = true LIMIT (?1)", nativeQuery = true)
    List<CollectionChord> findCollectionChordsWithAdminAuthorWithLimit(int limit);

}
