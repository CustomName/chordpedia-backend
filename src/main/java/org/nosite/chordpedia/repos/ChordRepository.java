package org.nosite.chordpedia.repos;

import org.nosite.chordpedia.model.Chord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ChordRepository extends CrudRepository<Chord, Long> {

    @Query(value = "SELECT * FROM chord c, keynote k, type_chord t\n" +
            "WHERE c.id_keynote = k.id_keynote\n" +
            "AND c.id_type_chord = t.id_type_chord\n" +
            "AND (LOWER(k.diez_code) = LOWER(?1) OR LOWER(k.bemol_code) = LOWER(?1))\n" +
            "AND LOWER(t.code_view) = LOWER(?2)", nativeQuery = true)
    List<Chord> findAllByKeynoteAndTypeChord(String keynoteCode, String typeChordCode);

    @Query(value = "SELECT * FROM chord WHERE id_chord IN (?1)", nativeQuery = true)
    List<Chord> findAllByIds(Integer[] ids);

    @Query(value = "SELECT * FROM chord WHERE id_keynote = ?1 AND id_type_chord IN (?2) AND is_difficult = false ORDER BY RANDOM() LIMIT 1;", nativeQuery = true)
    Optional<Chord> findRandChordByTonality(Long keynoteId, Integer[] typeChordIds);

    @Query(value = "SELECT * FROM chord ORDER BY RANDOM() LIMIT 1;", nativeQuery = true)
    Optional<Chord> findRandChord();

}
