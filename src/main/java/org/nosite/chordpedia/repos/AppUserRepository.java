package org.nosite.chordpedia.repos;

import org.nosite.chordpedia.model.AppUser;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AppUserRepository extends CrudRepository<AppUser, Long> {

    Optional<AppUser> findByLogin(String login);

}
