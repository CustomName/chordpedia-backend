package org.nosite.chordpedia.repos;

import org.nosite.chordpedia.model.Tonality;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TonalityRepository extends CrudRepository<Tonality, Long> {

    @Query(value = "SELECT * FROM tonality ORDER BY RANDOM() LIMIT 1;", nativeQuery = true)
    Optional<Tonality> findRandTonality();

}
