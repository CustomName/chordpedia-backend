package org.nosite.chordpedia.converters;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.dto.CollectionChordsDto;
import org.nosite.chordpedia.model.CollectionChord;
import org.nosite.chordpedia.services.ChordService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class CollectionChordsConverter {

    private final ChordService chordService;

    public CollectionChordsDto convert(CollectionChord collectionChord){
        Map<String, Object> groupChords = collectionChord.getGroupChordsData().entrySet().stream()
                .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            entry -> chordService.getChordsByIds((Integer[]) ((ArrayList) entry.getValue()).toArray(new Integer[0]))
                        ));

        return CollectionChordsDto.builder()
                .uuid(collectionChord.getUuid())
                .title(collectionChord.getTitle())
                .description(collectionChord.getDescription())
                .isPublic(collectionChord.isPublic())
                .groupChords(groupChords)
                .groupNames(collectionChord.getGroupNamesData())
                .build();
    }

}
