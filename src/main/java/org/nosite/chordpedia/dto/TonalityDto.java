package org.nosite.chordpedia.dto;

import lombok.*;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TonalityDto {

    private Long idTonality;

    private String name;

    private int orderTonality;

    private List<Long> keynoteIds;

    private Long idTonalityRules;

    private Map<String, Object> levelForKeynoteData;

}
