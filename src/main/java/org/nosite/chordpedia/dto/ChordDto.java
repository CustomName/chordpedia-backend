package org.nosite.chordpedia.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChordDto {

    private Long idChord;

    private String keynote;

    private short startFret;

    private boolean isDifficult;

    private short[] strings;

    private NoteDto[] notes;

    private BareDto[] bares;

    private String typeChordCode;

}
