package org.nosite.chordpedia.dto;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
public class AuthResp {

    private boolean result;

    private String token;

}
