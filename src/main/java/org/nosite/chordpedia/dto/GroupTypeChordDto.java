package org.nosite.chordpedia.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GroupTypeChordDto {

    private Long idGroupTypeChord;

    private String value;

    private int orderGroupTypeChord;

}
