package org.nosite.chordpedia.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class KeynoteDto {

    private Long idKeynote;

    private String code;

    private String diezCode;

    private String bemolCode;

    private String diezView;

    private String bemolView;

    private int orderKeynote;

}
