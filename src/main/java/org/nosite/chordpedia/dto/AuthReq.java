package org.nosite.chordpedia.dto;

import lombok.*;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AuthReq {

    private String login;

    private String password;

}
