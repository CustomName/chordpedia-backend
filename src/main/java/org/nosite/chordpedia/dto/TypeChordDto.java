package org.nosite.chordpedia.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TypeChordDto {

    private Long idTypeChord;

    private String code;

    private String codeView;

    private String value;

    private Long idGroupTypeChord;

    private int orderTypeChord;

}
