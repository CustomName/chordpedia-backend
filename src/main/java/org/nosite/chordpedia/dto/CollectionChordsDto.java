package org.nosite.chordpedia.dto;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CollectionChordsDto {

    private String uuid;

    private String title;

    private String description;

    private boolean isPublic;

    private Map<String, Object> groupChords;

    private Map<String, Object> groupNames;

}
