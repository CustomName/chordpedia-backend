package org.nosite.chordpedia.dto;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TonalityRulesDto {

    private Long idTonalityRules;

    private String name;

    private Map<String, Object> levelForChordTypeData;

}
