package org.nosite.chordpedia.model;

import lombok.*;
import org.nosite.chordpedia.converters.HashMapConverter;

import javax.persistence.*;
import java.util.Map;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Chord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idChord;

    @ManyToOne
    @JoinColumn(name = "id_keynote")
    private Keynote keynote;

    private short startFret;

    @Column(columnDefinition = "boolean default false")
    private boolean isDifficult;

    @ManyToOne
    @JoinColumn(name = "id_type_chord")
    private TypeChord typeChord;

    private String dataJSON;

    @Convert(converter = HashMapConverter.class)
    private Map<String, Object> data;

}
