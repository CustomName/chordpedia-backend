package org.nosite.chordpedia.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TypeChord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idTypeChord;

    private String code;

    private String codeView;

    private String value;

    @ManyToOne
    @JoinColumn(name = "id_group_type_chord")
    private GroupTypeChord groupTypeChord;

    private int orderTypeChord;

}
