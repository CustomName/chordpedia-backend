package org.nosite.chordpedia.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Keynote {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idKeynote;

    private String code;

    private String diezCode;

    private String bemolCode;

    private String diezView;

    private String bemolView;

    private int orderKeynote;

}
