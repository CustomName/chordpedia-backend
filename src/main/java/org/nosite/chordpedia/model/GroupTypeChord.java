package org.nosite.chordpedia.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GroupTypeChord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idGroupTypeChord;

    private String value;

    private int orderGroupTypeChord;

}
