package org.nosite.chordpedia.model;

import lombok.*;
import org.nosite.chordpedia.converters.HashMapConverter;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Tonality {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idTonality;

    private String name;

    private int orderTonality;

    @OneToMany
    private List<Keynote> keynotes;

    private String levelForKeynoteDataJSON;

    @Convert(converter = HashMapConverter.class)
    private Map<String, Object> levelForKeynoteData;

    @ManyToOne
    @JoinColumn(name = "id_tonality_rules")
    private TonalityRules tonalityRules;

}
