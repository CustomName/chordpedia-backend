package org.nosite.chordpedia.model;

import lombok.*;
import org.nosite.chordpedia.converters.HashMapConverter;

import javax.persistence.*;
import java.util.Map;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CollectionChord {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idCollectionChord;

    private String uuid;

    private String title;

    private String description;

    private boolean isAdminAuthor;

    private boolean isPublic;

    private String groupChordsDataJSON;

    @Convert(converter = HashMapConverter.class)
    private Map<String, Object> groupChordsData;

    private String groupNamesDataJSON;

    @Convert(converter = HashMapConverter.class)
    private Map<String, Object> groupNamesData;

}
