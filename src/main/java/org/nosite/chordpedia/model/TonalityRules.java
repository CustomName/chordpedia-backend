package org.nosite.chordpedia.model;

import lombok.*;
import org.nosite.chordpedia.converters.HashMapConverter;

import javax.persistence.*;
import java.util.Map;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TonalityRules {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idTonalityRules;

    private String name;

    private String levelForChordTypeDataJSON;

    @Convert(converter = HashMapConverter.class)
    private Map<String, Object> levelForChordTypeData;

}
