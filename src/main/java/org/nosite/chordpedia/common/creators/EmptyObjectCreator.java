package org.nosite.chordpedia.common.creators;

import org.nosite.chordpedia.model.*;
import org.springframework.stereotype.Component;

@Component
public class EmptyObjectCreator implements ObjectCreator {

    @Override
    public Chord chord() {
        return Chord.builder()
                .idChord(-1L)
                .build();
    }

    @Override
    public CollectionChord collectionChord() {
        return CollectionChord.builder()
                .idCollectionChord(-1L)
                .build();
    }

    @Override
    public GroupTypeChord groupTypeChord() {
        return GroupTypeChord.builder()
                .idGroupTypeChord(-1L)
                .build();
    }

    @Override
    public Keynote keynote() {
        return Keynote.builder()
                .idKeynote(-1L)
                .build();
    }

    @Override
    public TonalityRules tonalityRules() {
        return TonalityRules.builder()
                .idTonalityRules(-1L)
                .build();
    }

    @Override
    public Tonality tonality() {
        return Tonality.builder()
                .idTonality(-1L)
                .build();
    }

    @Override
    public TypeChord typeChord() {
        return TypeChord.builder()
                .idTypeChord(-1L)
                .build();
    }

}
