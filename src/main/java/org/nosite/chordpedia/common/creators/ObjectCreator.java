package org.nosite.chordpedia.common.creators;

import org.nosite.chordpedia.model.*;

public interface ObjectCreator {

    Chord chord();

    CollectionChord collectionChord();

    GroupTypeChord groupTypeChord();

    Keynote keynote();

    TonalityRules tonalityRules();

    Tonality tonality();

    TypeChord typeChord();

}
