package org.nosite.chordpedia.services;

import org.nosite.chordpedia.model.TypeChord;

import java.util.List;

public interface TypeChordService {

    TypeChord saveTypeChord(TypeChord typeChord);

    TypeChord getTypeChordByCode(String code);

    TypeChord getTypeChordByIdTypeChord(Long idTypeChord);

    void deleteTypeChordByIdTypeChord(Long idTypeChord);

    List<TypeChord> getTypeChords();

}
