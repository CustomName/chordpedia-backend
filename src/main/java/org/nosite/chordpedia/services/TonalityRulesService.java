package org.nosite.chordpedia.services;

import org.nosite.chordpedia.model.TonalityRules;

import java.util.List;

public interface TonalityRulesService {

    TonalityRules saveTonalityRules(TonalityRules tonalityRules);

    List<TonalityRules> getTonalityRulesList();

    TonalityRules getTonalityRulesById(Long idTonalityRules);

}
