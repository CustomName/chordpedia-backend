package org.nosite.chordpedia.services;

import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.model.Tonality;

import java.util.List;

public interface TonalityService {

    Tonality saveTonality(Tonality tonality);

    Tonality getTonalityById(Long idTonality);

    List<Tonality> getTonalities();

    Keynote getMainKeynote(Tonality tonality);

    Tonality getRandTonality();

}
