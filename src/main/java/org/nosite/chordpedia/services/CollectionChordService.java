package org.nosite.chordpedia.services;

import org.nosite.chordpedia.model.CollectionChord;

import java.util.List;

public interface CollectionChordService {

    void deleteCollection(CollectionChord collectionChord);

    List<CollectionChord> findAllCollectionChordsWithMinData();

    List<CollectionChord> findCollectionChordsWithAdminAuthorWithMinData(int limit);

    CollectionChord getCollectionChordByUuid(String uuid);

    CollectionChord saveCollectionChord(CollectionChord collectionChord);

}
