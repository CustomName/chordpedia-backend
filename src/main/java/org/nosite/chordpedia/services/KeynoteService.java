package org.nosite.chordpedia.services;

import org.nosite.chordpedia.model.Keynote;

import java.util.List;

public interface KeynoteService {

    Keynote saveKeynote(Keynote keynote);

    Keynote getKeynoteByCode(String code);

    Keynote getKeynoteByIdKeynote(Long idKeynote);

    void deleteKeynoteByIdKeynote(Long idKeynote);

    List<Keynote> getKeynotes();

    List<Keynote> getKeynotesByIds(List<Long> keynoteIds);

}
