package org.nosite.chordpedia.services;

import org.nosite.chordpedia.model.GroupTypeChord;

import java.util.List;

public interface GroupTypeChordService {

    GroupTypeChord saveGroupTypeChord(GroupTypeChord groupTypeChord);

    GroupTypeChord getGroupTypeChordById(Long idGroupTypeChord);

    void deleteGroupTypeChordById(Long idGroupTypeChord);

    List<GroupTypeChord> getGroupTypeChords();

}
