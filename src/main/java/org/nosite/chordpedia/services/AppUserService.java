package org.nosite.chordpedia.services;

import org.nosite.chordpedia.model.AppUser;

public interface AppUserService {

    AppUser findByLoginAndPassword(String login, String password);

    AppUser findByLogin(String login);

}
