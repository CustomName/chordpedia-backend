package org.nosite.chordpedia.services;

import org.nosite.chordpedia.model.Chord;

import java.util.List;

public interface ChordService {

    long getChordsCount();

    void deleteChordByIdChord(Long idChord);

    List<Chord> getAllChords();

    Chord getRandChord();

    List<Chord> getChordsByIds(Integer[] ids);

    List<Chord> findAllByKeynoteAndTypeChord(String value);

    Chord getRandChordByTonality(Long keynoteId, Integer[] typeChordIds);

    Chord getChordById(long idChord);

    Chord saveChord(Chord chord);

}
