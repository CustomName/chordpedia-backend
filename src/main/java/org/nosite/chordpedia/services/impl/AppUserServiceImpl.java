package org.nosite.chordpedia.services.impl;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.common.exceptions.UserNotFound;
import org.nosite.chordpedia.model.AppUser;
import org.nosite.chordpedia.repos.AppUserRepository;
import org.nosite.chordpedia.services.AppUserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class AppUserServiceImpl implements AppUserService {

    private final AppUserRepository appUserRepo;
    private final PasswordEncoder passwordEncoder;

    @Override
    public AppUser findByLoginAndPassword(String login, String password){
        Optional<AppUser> appUser = appUserRepo.findByLogin(login);

        appUser.orElseThrow(UserNotFound::new);

        if(passwordEncoder.matches(password, appUser.get().getPassword())){
            return appUser.get();
        }

        return null;
    }

    @Override
    public AppUser findByLogin(String login){
        Optional<AppUser> appUser = appUserRepo.findByLogin(login);

        return appUser.orElseThrow(UserNotFound::new);
    }

}
