package org.nosite.chordpedia.services.impl;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.common.creators.ObjectCreator;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.repos.KeynoteRepository;
import org.nosite.chordpedia.services.KeynoteService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class KeynoteServiceImpl implements KeynoteService {

    private final KeynoteRepository keynoteRepository;
    private final ObjectCreator objectCreator;

    @Override
    public Keynote saveKeynote(Keynote keynote){
        return keynoteRepository.save(keynote);
    }

    @Override
    public Keynote getKeynoteByCode(String code){
        Optional<Keynote> keynote = keynoteRepository.findByCode(code);

        return keynote.orElseGet(objectCreator::keynote);
    }

    @Override
    public Keynote getKeynoteByIdKeynote(Long idKeynote){
        Optional<Keynote> keynote = keynoteRepository.findById(idKeynote);

        return keynote.orElseGet(objectCreator::keynote);
    }

    @Override
    public void deleteKeynoteByIdKeynote(Long idKeynote){
        keynoteRepository.deleteById(idKeynote);
    }

    @Override
    public List<Keynote> getKeynotes(){
        List<Keynote> keynotes = new ArrayList<>();
        keynoteRepository.findAll().forEach(keynotes::add);
        return keynotes;
    }

    @Override
    public List<Keynote> getKeynotesByIds(List<Long> keynoteIds){
        List<Keynote> keynotes = new ArrayList<>();

        keynoteRepository.findAll().forEach(keynotes::add);

        return keynotes.stream()
                .filter(keynote -> keynoteIds.contains(keynote.getIdKeynote()))
                .collect(Collectors.toList());
    }

}
