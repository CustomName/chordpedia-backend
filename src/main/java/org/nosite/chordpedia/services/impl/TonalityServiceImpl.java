package org.nosite.chordpedia.services.impl;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.common.creators.ObjectCreator;
import org.nosite.chordpedia.converters.HashMapConverter;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.model.Tonality;
import org.nosite.chordpedia.repos.TonalityRepository;
import org.nosite.chordpedia.services.TonalityService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
@AllArgsConstructor
public class TonalityServiceImpl implements TonalityService {

    private final TonalityRepository tonalityRepository;
    private final HashMapConverter hashMapConverter;
    private final ObjectCreator objectCreator;

    @Override
    public Tonality saveTonality(Tonality tonality){
        tonality.setLevelForKeynoteDataJSON(hashMapConverter.convertToDatabaseColumn(tonality.getLevelForKeynoteData()));
        Tonality savedTonality = tonalityRepository.save(tonality);
        if(nonNull(savedTonality)){
            savedTonality.setLevelForKeynoteData(hashMapConverter.convertToEntityAttribute(savedTonality.getLevelForKeynoteDataJSON()));
        }

        return savedTonality;
    }

    @Override
    public Tonality getTonalityById(Long idTonality){
        Optional<Tonality> tonality = tonalityRepository.findById(idTonality);
        tonality.ifPresent(value -> {
            value.setLevelForKeynoteData(hashMapConverter.convertToEntityAttribute(value.getLevelForKeynoteDataJSON()));
        });

        return tonality.orElseGet(objectCreator::tonality);
    }

    @Override
    public List<Tonality> getTonalities(){
        List<Tonality> tonalities = new ArrayList<>();
        tonalityRepository.findAll().forEach(tonalities::add);
        tonalities.forEach(value -> {
            value.setLevelForKeynoteData(hashMapConverter.convertToEntityAttribute(value.getLevelForKeynoteDataJSON()));
        });

        return tonalities;
    }

    @Override
    public Keynote getMainKeynote(Tonality tonality){
        Map.Entry<String, Object> levelMainKeynote = tonality.getLevelForKeynoteData().entrySet().stream()
                .filter(entry -> entry.getValue().equals(1))
                .findFirst().orElse(null);
        if(isNull(levelMainKeynote)) return objectCreator.keynote();

        long idKeynote = Long.parseLong(levelMainKeynote.getKey());

        return tonality.getKeynotes().stream()
                .filter(keynote -> keynote.getIdKeynote() == idKeynote)
                .findFirst().orElseGet(objectCreator::keynote);
    }

    @Override
    public Tonality getRandTonality(){
        Optional<Tonality> tonality = tonalityRepository.findRandTonality();
        tonality.ifPresent(value -> {
            value.setLevelForKeynoteData(hashMapConverter.convertToEntityAttribute(value.getLevelForKeynoteDataJSON()));
        });

        return tonality.orElseGet(objectCreator::tonality);
    }

}
