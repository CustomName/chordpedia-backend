package org.nosite.chordpedia.services.impl;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.common.creators.ObjectCreator;
import org.nosite.chordpedia.converters.HashMapConverter;
import org.nosite.chordpedia.model.CollectionChord;
import org.nosite.chordpedia.repos.CollectionChordRepository;
import org.nosite.chordpedia.services.CollectionChordService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Service
@AllArgsConstructor
public class CollectionChordServiceImpl implements CollectionChordService {

    private final CollectionChordRepository collectionChordRepository;
    private final HashMapConverter hashMapConverter;
    private final ObjectCreator objectCreator;

    @Override
    public CollectionChord saveCollectionChord(CollectionChord collectionChord){
        collectionChord.setGroupChordsDataJSON(hashMapConverter.convertToDatabaseColumn(collectionChord.getGroupChordsData()));
        collectionChord.setGroupNamesDataJSON(hashMapConverter.convertToDatabaseColumn(collectionChord.getGroupNamesData()));

        CollectionChord savedCollectionChord = collectionChordRepository.save(collectionChord);
        if(nonNull(savedCollectionChord)){
            savedCollectionChord.setGroupChordsData(hashMapConverter.convertToEntityAttribute(savedCollectionChord.getGroupChordsDataJSON()));
            savedCollectionChord.setGroupNamesData(hashMapConverter.convertToEntityAttribute(savedCollectionChord.getGroupNamesDataJSON()));
        }

        return savedCollectionChord;
    }

    @Override
    public CollectionChord getCollectionChordByUuid(String uuid){
        Optional<CollectionChord> collectionChord = collectionChordRepository.findByUuid(uuid);

        collectionChord.ifPresent(value -> {
            value.setGroupChordsData(hashMapConverter.convertToEntityAttribute(value.getGroupChordsDataJSON()));
            value.setGroupNamesData(hashMapConverter.convertToEntityAttribute(value.getGroupNamesDataJSON()));
        });

        return collectionChord.orElseGet(objectCreator::collectionChord);
    }

    @Override
    public List<CollectionChord> findCollectionChordsWithAdminAuthorWithMinData(int limit){
        return limit <= 0 ? collectionChordRepository.findCollectionChordsWithAdminAuthor()
                : collectionChordRepository.findCollectionChordsWithAdminAuthorWithLimit(limit);
    }

    @Override
    public List<CollectionChord> findAllCollectionChordsWithMinData(){
        List<CollectionChord> collectionChords = new ArrayList<>();
        collectionChordRepository.findAll().forEach(collectionChords::add);

        return collectionChords;
    }

    @Override
    public void deleteCollection(CollectionChord collectionChord){
        collectionChordRepository.delete(collectionChord);
    }

}
