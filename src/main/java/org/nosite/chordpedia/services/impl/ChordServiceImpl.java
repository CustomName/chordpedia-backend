package org.nosite.chordpedia.services.impl;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.common.creators.ObjectCreator;
import org.nosite.chordpedia.converters.HashMapConverter;
import org.nosite.chordpedia.model.Chord;
import org.nosite.chordpedia.repos.ChordRepository;
import org.nosite.chordpedia.services.ChordService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class ChordServiceImpl implements ChordService {

    private final ChordRepository chordRepository;
    private final HashMapConverter hashMapConverter;
    private final ObjectCreator objectCreator;

    @Override
    @Caching(put = {
        @CachePut(value = "chordCache", key = "#chord.idChord")
    }, evict = {
        @CacheEvict(value = {
            "allChordsCache",
            "chordsByIdsCache",
            "chordsByKeynoteAndTypeChordCache"}, allEntries = true)
    })
    public Chord saveChord(Chord chord){
        chord.setDataJSON(hashMapConverter.convertToDatabaseColumn(chord.getData()));

        Chord savedChord = chordRepository.save(chord);
        if (nonNull(savedChord)){
            savedChord.setData(hashMapConverter.convertToEntityAttribute(savedChord.getDataJSON()));
        }

        return savedChord;
    }

    @Override
    @Cacheable(value = "chordCache", key = "#idChord")
    public Chord getChordById(long idChord){
        Optional<Chord> chord = chordRepository.findById(idChord);

        chord.ifPresent(value -> value.setData(hashMapConverter.convertToEntityAttribute(value.getDataJSON())));

        return chord.orElseGet(objectCreator::chord);
    }

    @Override
    public Chord getRandChordByTonality(Long keynoteId, Integer[] typeChordIds){
        Optional<Chord> chord = chordRepository.findRandChordByTonality(keynoteId, typeChordIds);

        chord.ifPresent(value -> value.setData(hashMapConverter.convertToEntityAttribute(value.getDataJSON())));

        return chord.orElseGet(objectCreator::chord);
    }

    @Override
    @Cacheable("chordsByKeynoteAndTypeChordCache")
    public List<Chord> findAllByKeynoteAndTypeChord(String value){
        final String keynoteCode;
        final String typeChordCode;
        if(value.length() > 1 &&
                (value.toCharArray()[1] == '#' || value.toCharArray()[1] == 'b')){
            keynoteCode = value.substring(0, 2);
            typeChordCode = value.substring(2);
        } else{
            keynoteCode = value.substring(0, 1);
            typeChordCode = value.substring(1);
        }

        List<Chord> chords = chordRepository.findAllByKeynoteAndTypeChord(keynoteCode, typeChordCode);

        chords.forEach(val -> val.setData(hashMapConverter.convertToEntityAttribute(val.getDataJSON())));

        return chords;
    }

    @Override
    @Cacheable("chordsByIdsCache")
    public List<Chord> getChordsByIds(Integer[] ids){
        List<Long> idOrder = Stream.of(ids).map(Long::valueOf).collect(toList());
        List<Chord> chords = chordRepository.findAllByIds(ids);
        chords.sort(Comparator.comparingInt(chord -> (idOrder.indexOf(chord.getIdChord()))));

        chords.forEach(val -> val.setData(hashMapConverter.convertToEntityAttribute(val.getDataJSON())));

        return chords;
    }

    @Override
    public Chord getRandChord(){
        Optional<Chord> chord = chordRepository.findRandChord();

        chord.ifPresent(value -> value.setData(hashMapConverter.convertToEntityAttribute(value.getDataJSON())));

        return chord.orElseGet(objectCreator::chord);
    }

    @Override
    @Cacheable("allChordsCache")
    public List<Chord> getAllChords(){
        List<Chord> allChords = new ArrayList<>();

        chordRepository.findAll().forEach(allChords::add);

        return allChords;
    }

    @Override
    @Caching(
        evict = {
            @CacheEvict(value = {
                "allChordsCache",
                "chordsByIdsCache",
                "chordsByKeynoteAndTypeChordCache"}, allEntries = true),
            @CacheEvict(value = "chordCache", key = "#idChord")
        }
    )
    public void deleteChordByIdChord(Long idChord){
        chordRepository.deleteById(idChord);
    }

    @Override
    public long getChordsCount(){
        return chordRepository.count();
    }

}
