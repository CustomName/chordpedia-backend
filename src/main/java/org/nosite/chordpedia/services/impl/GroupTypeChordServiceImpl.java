package org.nosite.chordpedia.services.impl;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.common.creators.ObjectCreator;
import org.nosite.chordpedia.model.GroupTypeChord;
import org.nosite.chordpedia.repos.GroupTypeChordRepository;
import org.nosite.chordpedia.services.GroupTypeChordService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GroupTypeChordServiceImpl implements GroupTypeChordService {

    private final GroupTypeChordRepository groupTypeChordRepository;
    private final ObjectCreator objectCreator;

    @Override
    public GroupTypeChord saveGroupTypeChord(GroupTypeChord groupTypeChord){
        return groupTypeChordRepository.save(groupTypeChord);
    }

    @Override
    public GroupTypeChord getGroupTypeChordById(Long idGroupTypeChord){
        Optional<GroupTypeChord> groupTypeChord = groupTypeChordRepository.findById(idGroupTypeChord);

        return groupTypeChord.orElseGet(objectCreator::groupTypeChord);
    }

    @Override
    public void deleteGroupTypeChordById(Long idGroupTypeChord){
        groupTypeChordRepository.deleteById(idGroupTypeChord);
    }

    @Override
    public List<GroupTypeChord> getGroupTypeChords(){
        List<GroupTypeChord> groupTypeChords = new ArrayList<>();
        groupTypeChordRepository.findAll().forEach(groupTypeChords::add);
        return groupTypeChords;
    }

}
