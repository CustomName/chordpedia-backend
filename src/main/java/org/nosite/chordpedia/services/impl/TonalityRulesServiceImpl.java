package org.nosite.chordpedia.services.impl;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.common.creators.ObjectCreator;
import org.nosite.chordpedia.converters.HashMapConverter;
import org.nosite.chordpedia.model.TonalityRules;
import org.nosite.chordpedia.repos.TonalityRulesRepository;
import org.nosite.chordpedia.services.TonalityRulesService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Service
@AllArgsConstructor
public class TonalityRulesServiceImpl implements TonalityRulesService {

    private final TonalityRulesRepository tonalityRulesRepository;
    private final HashMapConverter hashMapConverter;
    private final ObjectCreator objectCreator;

    @Override
    public TonalityRules saveTonalityRules(TonalityRules tonalityRules){
        tonalityRules.setLevelForChordTypeDataJSON(hashMapConverter.convertToDatabaseColumn(tonalityRules.getLevelForChordTypeData()));
        TonalityRules savedTonalityRules = tonalityRulesRepository.save(tonalityRules);
        if(nonNull(savedTonalityRules)){
            savedTonalityRules.setLevelForChordTypeData(hashMapConverter.convertToEntityAttribute(savedTonalityRules.getLevelForChordTypeDataJSON()));
        }

        return savedTonalityRules;
    }

    @Override
    public List<TonalityRules> getTonalityRulesList(){
        List<TonalityRules> tonalityRulesList = new ArrayList<>();
        tonalityRulesRepository.findAll().forEach(tonalityRulesList::add);

        return tonalityRulesList;
    }

    @Override
    public TonalityRules getTonalityRulesById(Long idTonalityRules){
        Optional<TonalityRules> tonalityRules = tonalityRulesRepository.findById(idTonalityRules);
        tonalityRules.ifPresent(rules ->
                rules.setLevelForChordTypeData(hashMapConverter.convertToEntityAttribute(rules.getLevelForChordTypeDataJSON())));

        return tonalityRules.orElseGet(objectCreator::tonalityRules);
    }

}
