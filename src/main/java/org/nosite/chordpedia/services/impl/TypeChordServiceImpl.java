package org.nosite.chordpedia.services.impl;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.common.creators.ObjectCreator;
import org.nosite.chordpedia.model.TypeChord;
import org.nosite.chordpedia.repos.TypeChordRepository;
import org.nosite.chordpedia.services.TypeChordService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class TypeChordServiceImpl implements TypeChordService {

    private final TypeChordRepository typeChordRepository;
    private final ObjectCreator objectCreator;

    @Override
    public TypeChord saveTypeChord(TypeChord typeChord){
        return typeChordRepository.save(typeChord);
    }

    @Override
    public TypeChord getTypeChordByCode(String code){
        Optional<TypeChord> typeChord = typeChordRepository.findByCode(code);

        return typeChord.orElseGet(objectCreator::typeChord);
    }

    @Override
    public TypeChord getTypeChordByIdTypeChord(Long idTypeChord){
        Optional<TypeChord> typeChord = typeChordRepository.findById(idTypeChord);

        return typeChord.orElseGet(objectCreator::typeChord);
    }

    @Override
    public void deleteTypeChordByIdTypeChord(Long idTypeChord){
        typeChordRepository.deleteById(idTypeChord);
    }

    @Override
    public List<TypeChord> getTypeChords(){
        List<TypeChord> typeChords = new ArrayList<>();
        typeChordRepository.findAll().forEach(typeChords::add);
        return typeChords;
    }

}
