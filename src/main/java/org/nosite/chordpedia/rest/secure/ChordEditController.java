package org.nosite.chordpedia.rest.secure;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.dto.ChordDto;
import org.nosite.chordpedia.dto.ResultResp;
import org.nosite.chordpedia.model.Chord;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.model.TypeChord;
import org.nosite.chordpedia.services.ChordService;
import org.nosite.chordpedia.services.KeynoteService;
import org.nosite.chordpedia.services.TypeChordService;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/private")
@Slf4j
@AllArgsConstructor
public class ChordEditController {

    private final ChordService chordService;
    private final TypeChordService typeChordService;
    private final KeynoteService keynoteService;

    @PutMapping("/chord")
    public ResultResp createChord(@RequestBody ChordDto chordDto){
        TypeChord typeChord = typeChordService.getTypeChordByCode(chordDto.getTypeChordCode());
        Keynote keynote = keynoteService.getKeynoteByCode(chordDto.getKeynote());

        Map<String, Object> data = new HashMap<>();
        data.put("strings", chordDto.getStrings());
        data.put("notes", chordDto.getNotes());
        data.put("bares", chordDto.getBares());

        Chord chord = Chord.builder()
                .idChord(chordDto.getIdChord())
                .keynote(keynote)
                .startFret(chordDto.getStartFret())
                .isDifficult(chordDto.isDifficult())
                .data(data)
                .typeChord(typeChord)
                .build();

        Chord savedChord = chordService.saveChord(chord);

        if(nonNull(savedChord)){
            return ResultResp.builder()
                    .result(true)
                    .msg(savedChord.toString())
                    .build();
        }

        return ResultResp.builder()
                .build();
    }

    @DeleteMapping("/chord/{idChord}/delete")
    public ResultResp deleteChord(@PathVariable("idChord") Long idChord){
        Chord chord = chordService.getChordById(idChord);

        if(nonNull(chord)){
            chordService.deleteChordByIdChord(chord.getIdChord());
            return ResultResp.builder().result(true).msg(String.format("Аккорд \"%s\" удален", chord.getIdChord())).build();
        }

        return ResultResp.builder()
                .build();
    }

}
