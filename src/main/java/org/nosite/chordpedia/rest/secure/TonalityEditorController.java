package org.nosite.chordpedia.rest.secure;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.dto.ResultResp;
import org.nosite.chordpedia.dto.TonalityDto;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.model.Tonality;
import org.nosite.chordpedia.model.TonalityRules;
import org.nosite.chordpedia.services.KeynoteService;
import org.nosite.chordpedia.services.TonalityRulesService;
import org.nosite.chordpedia.services.TonalityService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/private")
@Slf4j
@AllArgsConstructor
public class TonalityEditorController {

    private final TonalityService tonalityService;
    private final KeynoteService keynoteService;
    private final TonalityRulesService tonalityRulesService;

    @PutMapping("/tonality")
    public ResultResp createTonality(@RequestBody TonalityDto tonalityDto){
        List<Keynote> keynotes = keynoteService.getKeynotesByIds(tonalityDto.getKeynoteIds());
        TonalityRules tonalityRules = tonalityRulesService.getTonalityRulesById(tonalityDto.getIdTonalityRules());

        Tonality tonality = Tonality.builder()
                .idTonality(tonalityDto.getIdTonality())
                .name(tonalityDto.getName())
                .orderTonality(tonalityDto.getOrderTonality())
                .keynotes(keynotes)
                .tonalityRules(tonalityRules)
                .levelForKeynoteData(tonalityDto.getLevelForKeynoteData())
                .build();

        Tonality savedTonality = tonalityService.saveTonality(tonality);

        if(nonNull(savedTonality)){
            return ResultResp.builder()
                    .result(true)
                    .msg(savedTonality.toString())
                    .build();
        }

        return ResultResp.builder()
                .build();
    }

}
