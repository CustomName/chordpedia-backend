package org.nosite.chordpedia.rest.secure;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.dto.ResultResp;
import org.nosite.chordpedia.dto.TypeChordDto;
import org.nosite.chordpedia.model.GroupTypeChord;
import org.nosite.chordpedia.model.TypeChord;
import org.nosite.chordpedia.services.GroupTypeChordService;
import org.nosite.chordpedia.services.TypeChordService;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/private")
@Slf4j
@AllArgsConstructor
public class TypeChordEditorController {

    private final TypeChordService typeChordService;
    private final GroupTypeChordService groupTypeChordService;

    @PutMapping("/type-chord")
    public ResultResp createTypeChord(@RequestBody TypeChordDto typeChordDto){
        GroupTypeChord groupTypeChord = groupTypeChordService.getGroupTypeChordById(typeChordDto.getIdGroupTypeChord());

        TypeChord typeChord = TypeChord.builder()
                .idTypeChord(typeChordDto.getIdTypeChord())
                .code(typeChordDto.getCode())
                .codeView(typeChordDto.getCodeView())
                .value(typeChordDto.getValue())
                .groupTypeChord(groupTypeChord)
                .orderTypeChord(typeChordDto.getOrderTypeChord())
                .build();

        TypeChord savedTypeChord = typeChordService.saveTypeChord(typeChord);

        if(nonNull(savedTypeChord)){
            return ResultResp.builder()
                    .result(true)
                    .msg(savedTypeChord.toString())
                    .build();
        }

        return ResultResp.builder()
                .build();
    }

    @DeleteMapping("/type-chord/{idTypeChord}/delete")
    public ResultResp deleteTypeChord(@PathVariable("idTypeChord") Long idTypeChord){
        TypeChord typeChord = typeChordService.getTypeChordByIdTypeChord(idTypeChord);

        if(nonNull(typeChord)){
            typeChordService.deleteTypeChordByIdTypeChord(typeChord.getIdTypeChord());
            return ResultResp.builder().result(true).msg(String.format("тип аккорда \"%s\" удален", typeChord.getValue())).build();
        }

        return ResultResp.builder()
                .build();
    }

}
