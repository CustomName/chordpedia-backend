package org.nosite.chordpedia.rest.secure;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.dto.ResultResp;
import org.nosite.chordpedia.model.CollectionChord;
import org.nosite.chordpedia.services.CollectionChordService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/private")
@AllArgsConstructor
public class CollectionChordsEditController {

    private final CollectionChordService collectionChordService;

    @DeleteMapping("/collection/{uuid}/delete")
    public ResultResp deleteCollection(@PathVariable("uuid") String uuid){
        CollectionChord collection = collectionChordService.getCollectionChordByUuid(uuid);

        if(nonNull(collection)){
            collectionChordService.deleteCollection(collection);
            return ResultResp.builder().result(true).msg(String.format("Коллекция \"%s\" удалена", collection.getUuid())).build();
        }

        return ResultResp.builder()
                .build();
    }

    @GetMapping("/collections")
    public List<CollectionChord> getCollectionChords(){
        return collectionChordService.findAllCollectionChordsWithMinData();
    }

}
