package org.nosite.chordpedia.rest.secure;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.dto.KeynoteDto;
import org.nosite.chordpedia.dto.ResultResp;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.services.KeynoteService;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/private")
@Slf4j
@AllArgsConstructor
public class KeynoteEditorController {

    private final KeynoteService keynoteService;

    @PutMapping("/keynote")
    public ResultResp createKeynote(@RequestBody KeynoteDto keynoteDto){
        Keynote keynote = Keynote.builder()
                .idKeynote(keynoteDto.getIdKeynote())
                .code(keynoteDto.getCode())
                .diezCode(keynoteDto.getDiezCode())
                .bemolCode(keynoteDto.getBemolCode())
                .diezView(keynoteDto.getDiezView())
                .bemolView(keynoteDto.getBemolView())
                .orderKeynote(keynoteDto.getOrderKeynote())
                .build();

        Keynote savedKeynote = keynoteService.saveKeynote(keynote);

        if(nonNull(savedKeynote)){
            return ResultResp.builder()
                    .result(true)
                    .msg(savedKeynote.toString())
                    .build();
        }

        return ResultResp.builder()
                .build();
    }

    @DeleteMapping("/keynote/{idKeynote}/delete")
    public ResultResp deleteTypeChord(@PathVariable("idKeynote") Long idKeynote){
        Keynote keynote = keynoteService.getKeynoteByIdKeynote(idKeynote);

        if(nonNull(keynote)){
            keynoteService.deleteKeynoteByIdKeynote(keynote.getIdKeynote());
            return ResultResp.builder().result(true).msg(String.format("Нота \"%s\" удалена", keynote.getCode())).build();
        }

        return ResultResp.builder()
                .build();
    }

}
