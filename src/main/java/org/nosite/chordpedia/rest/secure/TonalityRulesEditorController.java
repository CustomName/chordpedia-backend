package org.nosite.chordpedia.rest.secure;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.dto.ResultResp;
import org.nosite.chordpedia.dto.TonalityRulesDto;
import org.nosite.chordpedia.model.TonalityRules;
import org.nosite.chordpedia.services.TonalityRulesService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/private")
@Slf4j
@AllArgsConstructor
public class TonalityRulesEditorController {

    private final TonalityRulesService tonalityRulesService;

    @PutMapping("/tonality-rules")
    public ResultResp createTonalityRules(@RequestBody TonalityRulesDto tonalityRulesDto){
        TonalityRules tonalityRules = TonalityRules.builder()
                .idTonalityRules(tonalityRulesDto.getIdTonalityRules())
                .name(tonalityRulesDto.getName())
                .levelForChordTypeData(tonalityRulesDto.getLevelForChordTypeData())
                .build();

        TonalityRules savedTonalityRules = tonalityRulesService.saveTonalityRules(tonalityRules);

        if(nonNull(savedTonalityRules)){
            return ResultResp.builder()
                    .result(true)
                    .msg(savedTonalityRules.toString())
                    .build();
        }

        return ResultResp.builder()
                .build();
    }

    @GetMapping("/tonality-rules-list")
    public List<TonalityRules> getTonalityRulesList(){
        return tonalityRulesService.getTonalityRulesList();
    }

    @GetMapping("/tonality-rules-by-id/{idTonalityRules}")
    public TonalityRules getTonalityRules(@PathVariable("idTonalityRules") Long idTonalityRules){
        return tonalityRulesService.getTonalityRulesById(idTonalityRules);
    }

}
