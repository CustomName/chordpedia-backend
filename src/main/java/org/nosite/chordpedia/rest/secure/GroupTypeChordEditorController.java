package org.nosite.chordpedia.rest.secure;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.dto.GroupTypeChordDto;
import org.nosite.chordpedia.dto.ResultResp;
import org.nosite.chordpedia.model.GroupTypeChord;
import org.nosite.chordpedia.services.GroupTypeChordService;
import org.springframework.web.bind.annotation.*;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/api/private")
@Slf4j
@AllArgsConstructor
public class GroupTypeChordEditorController {

    private final GroupTypeChordService groupTypeChordService;

    @PutMapping("/group-type-chord")
    public ResultResp createGroupTypeChord(@RequestBody GroupTypeChordDto groupTypeChordDto){
        GroupTypeChord groupTypeChord = GroupTypeChord.builder()
                .idGroupTypeChord(groupTypeChordDto.getIdGroupTypeChord())
                .value(groupTypeChordDto.getValue())
                .orderGroupTypeChord(groupTypeChordDto.getOrderGroupTypeChord())
                .build();

        GroupTypeChord savedGroupTypeChord = groupTypeChordService.saveGroupTypeChord(groupTypeChord);

        if(nonNull(savedGroupTypeChord)){
            return ResultResp.builder()
                    .result(true)
                    .msg(savedGroupTypeChord.toString())
                    .build();
        }

        return ResultResp.builder()
                .build();
    }

    @DeleteMapping("/group-type-chord/{idGroupTypeChord}/delete")
    public ResultResp deleteGroupTypeChord(@PathVariable("idGroupTypeChord") Long idGroupTypeChord){
        GroupTypeChord groupTypeChord = groupTypeChordService.getGroupTypeChordById(idGroupTypeChord);

        if(nonNull(groupTypeChord)){
            groupTypeChordService.deleteGroupTypeChordById(groupTypeChord.getIdGroupTypeChord());
            return ResultResp.builder().result(true).msg(String.format("группа типа аккорда \"%s\" удалена", groupTypeChord.getValue())).build();
        }

        return ResultResp.builder()
                .build();
    }

}
