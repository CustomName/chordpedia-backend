package org.nosite.chordpedia.rest.open;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.model.Tonality;
import org.nosite.chordpedia.services.TonalityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/public")
@Slf4j
@AllArgsConstructor
public class TonalityController {

    private final TonalityService tonalityService;

    @GetMapping("/tonality-by-id/{idTonality}")
    public Tonality getTonalityByIdTonality(@PathVariable("idTonality") Long idTonality){
        Tonality tonality = tonalityService.getTonalityById(idTonality);

        return tonality;
    }

    @GetMapping("/tonalities")
    public List<Tonality> getTonalities(){
        List<Tonality> tonalities = tonalityService.getTonalities();
        tonalities.sort(Comparator.comparing(Tonality::getOrderTonality));

        return tonalities;
    }

}
