package org.nosite.chordpedia.rest.open;

import lombok.AllArgsConstructor;
import org.nosite.chordpedia.converters.CollectionChordsConverter;
import org.nosite.chordpedia.dto.CollectionChordsDto;
import org.nosite.chordpedia.dto.ResultResp;
import org.nosite.chordpedia.model.CollectionChord;
import org.nosite.chordpedia.jwt.JwtProvider;
import org.nosite.chordpedia.services.CollectionChordService;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.http.util.TextUtils.isBlank;
import static org.apache.http.util.TextUtils.isEmpty;
import static org.nosite.chordpedia.utils.Values.getUuid;

@RestController
@RequestMapping("/api/public")
@AllArgsConstructor
public class CollectionChordsController {

    private final CollectionChordService collectionChordService;
    private final CollectionChordsConverter collectionChordsConverter;
    private final JwtProvider jwtProvider;

    @PutMapping("/collection")
    public ResultResp createCollection(@RequestBody CollectionChordsDto collectionChordsDto,
                                       @RequestHeader("Authorization") String token){
        boolean isAdminAuthor = isEmpty(token) ? false : jwtProvider.validateToken(token);

        String uuid = isBlank(collectionChordsDto.getUuid())
                ? getUuid()
                : collectionChordsDto.getUuid();

        Long idCollectionChord = null;
        if(!isBlank(collectionChordsDto.getUuid())) {
            CollectionChord collectionChordByUuid = collectionChordService.getCollectionChordByUuid(collectionChordsDto.getUuid());
            if(nonNull(collectionChordByUuid)){
                idCollectionChord = collectionChordByUuid.getIdCollectionChord();
            }
        }

        CollectionChord collectionChord = CollectionChord.builder()
                .idCollectionChord(idCollectionChord)
                .uuid(uuid)
                .title(collectionChordsDto.getTitle())
                .description(collectionChordsDto.getDescription())
                .isPublic(true)
                .isAdminAuthor(isAdminAuthor)
                .groupChordsData(collectionChordsDto.getGroupChords())
                .groupNamesData(collectionChordsDto.getGroupNames())
                .build();

        CollectionChord savedCollectionChord = collectionChordService.saveCollectionChord(collectionChord);

        if(nonNull(savedCollectionChord)){
            return ResultResp.builder()
                    .result(true)
                    .msg(savedCollectionChord.getUuid())
                    .build();
        }

        return ResultResp.builder()
                .build();
    }

    @GetMapping("/collection/{uuid}")
    public CollectionChordsDto getCollectionChordByUuid(@PathVariable ("uuid") String uuid){
        CollectionChord collectionChord = collectionChordService.getCollectionChordByUuid(uuid);

        if(isNull(collectionChord) || !collectionChord.isPublic()){
            return CollectionChordsDto.builder()
                    .title("Подборка аккордов")
                    .description(String.format("Подборка \"%s\" не найдена", uuid))
                    .groupChords(Collections.emptyMap())
                    .groupChords(Collections.emptyMap())
                    .build();
        }

        return collectionChordsConverter.convert(collectionChord);
    }

    @GetMapping("/collections")
    public List<CollectionChord> getCollectionChords(@RequestParam ("limit") int limit){
        return collectionChordService.findCollectionChordsWithAdminAuthorWithMinData(limit);
    }

}
