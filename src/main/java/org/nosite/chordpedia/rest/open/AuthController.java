package org.nosite.chordpedia.rest.open;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.dto.AuthReq;
import org.nosite.chordpedia.dto.AuthResp;
import org.nosite.chordpedia.dto.TokenReq;
import org.nosite.chordpedia.model.AppUser;
import org.nosite.chordpedia.jwt.JwtProvider;
import org.nosite.chordpedia.services.AppUserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/public")
@Slf4j
@AllArgsConstructor
public class AuthController {

	private final AppUserService appUserService;
	private final JwtProvider jwtProvider;

	@PostMapping("/auth")
	public AuthResp auth(@RequestBody AuthReq authReq){
		AppUser appUser = appUserService.findByLoginAndPassword(authReq.getLogin(), authReq.getPassword());
		if(appUser == null){
			return AuthResp.builder()
					.result(false)
					.build();
		}
		String token = jwtProvider.generateToken(appUser.getLogin());

		return AuthResp.builder()
                .result(true)
				.token(token)
				.build();
	}

	@PostMapping("/token")
	public AuthResp token(@RequestBody TokenReq tokenReq){
		boolean valid = jwtProvider.validateToken(tokenReq.getToken());

		return AuthResp.builder()
				.result(valid)
				.token(tokenReq.getToken())
				.build();
	}

}
