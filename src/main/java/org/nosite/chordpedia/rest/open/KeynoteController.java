package org.nosite.chordpedia.rest.open;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.services.KeynoteService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/public")
@Slf4j
@AllArgsConstructor
public class KeynoteController {

    private final KeynoteService keynoteService;

    @GetMapping("/keynote/{code}")
    public Keynote getKeynoteByCode(@PathVariable("code") String code){
        Keynote keynote = keynoteService.getKeynoteByCode(code);

        return keynote;
    }

    @GetMapping("/keynote-by-id/{idKeynote}")
    public Keynote getKeynoteByIdKeynote(@PathVariable("idKeynote") Long idKeynote){
        Keynote keynote = keynoteService.getKeynoteByIdKeynote(idKeynote);

        return keynote;
    }

    @GetMapping("/keynotes")
    public List<Keynote> getKeynotes(){
        List<Keynote> keynotes = keynoteService.getKeynotes();
        keynotes.sort(Comparator.comparing(Keynote::getOrderKeynote));

        return keynotes;
    }

}
