package org.nosite.chordpedia.rest.open;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.model.GroupTypeChord;
import org.nosite.chordpedia.services.GroupTypeChordService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;

@RestController
@RequestMapping("/api/public")
@Slf4j
@AllArgsConstructor
public class GroupTypeChordController {

    private final GroupTypeChordService groupTypeChordService;

    @GetMapping("/group-type-chord-by-id/{idGroupTypeChord}")
    public GroupTypeChord getGroupTypeChordById(@PathVariable("idGroupTypeChord") Long idGroupTypeChord){
        GroupTypeChord groupTypeChord = groupTypeChordService.getGroupTypeChordById(idGroupTypeChord);

        return groupTypeChord;
    }

    @GetMapping("/group-type-chords")
    public List<GroupTypeChord> getGroupTypeChords(){
        List<GroupTypeChord> groupTypeChords = groupTypeChordService.getGroupTypeChords();
        groupTypeChords.sort(Comparator.comparing(GroupTypeChord::getOrderGroupTypeChord));

        return groupTypeChords;
    }

}
