package org.nosite.chordpedia.rest.open;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.model.Chord;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.model.Tonality;
import org.nosite.chordpedia.services.ChordService;
import org.nosite.chordpedia.services.TonalityService;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/public")
@Slf4j
@AllArgsConstructor
public class ChordController {

    private final ChordService chordService;
    private final TonalityService tonalityService;
    private final Random random;

    @GetMapping("/chords/count")
    public long getChordsCount(){
        return chordService.getChordsCount();
    }

    @GetMapping("/chord/{idChord}")
    public Chord getChord(@PathVariable("idChord") Long idChord){
        return chordService.getChordById(idChord);
    }

    @GetMapping("/chords/{value}")
    public List<Chord> getChordsByValue(@PathVariable("value") String value){
        List<Chord> chords = chordService.findAllByKeynoteAndTypeChord(value);
        chords.sort(Comparator.comparing(Chord::getStartFret));

        return chords;
    }

    @GetMapping("/chord/random/{idTonality}")
    public Chord getRandChordByTonality(@PathVariable("idTonality") Long idTonality,
                                        @RequestParam("isFirst") boolean isFirst){
        Tonality tonality = tonalityService.getTonalityById(idTonality);
        Map<String, Object> levelForChordType = tonality.getTonalityRules().getLevelForChordTypeData();
        Keynote keynote;
        if(isFirst){
            keynote = tonalityService.getMainKeynote(tonality);
        } else {
            keynote = tonality.getKeynotes().get(random.nextInt(tonality.getKeynotes().size()));
        }
        Chord chord = getRandChord(levelForChordType, tonality, keynote);

        return chord;
    }

    @GetMapping("/chord/promo")
    public Chord getPromoChord(){
        return chordService.getRandChord();
    }

    @GetMapping("/chords/promo")
    public List<Chord> getPromoChords(){
        List<Chord> chords = new ArrayList<>();
        Tonality tonality = tonalityService.getRandTonality();
        Map<String, Object> levelForChordType = tonality.getTonalityRules().getLevelForChordTypeData();
        Keynote keynoteFirst = tonalityService.getMainKeynote(tonality);
        Keynote keynoteLast = tonality.getKeynotes().get(random.nextInt(tonality.getKeynotes().size()));

        chords.add(getRandChord(levelForChordType, tonality, keynoteFirst));
        chords.add(getRandChord(levelForChordType, tonality, keynoteLast));
        chords.add(getRandChord(levelForChordType, tonality, keynoteLast));

        return chords;
    }

    @GetMapping("/chords/ids")
    public List<Long> getChordIdsAll(){
        return chordService.getAllChords().stream()
                .map(Chord::getIdChord)
                .collect(Collectors.toList());
    }

    private Chord getRandChord(Map<String, Object> levelForChordType, Tonality tonality, Keynote keynote){
        //todo why so integer? how to do long
        List<Integer> typeChordIds = (List<Integer>) levelForChordType.get(String.valueOf(tonality.getLevelForKeynoteData().get(String.valueOf(keynote.getIdKeynote()))));

        return chordService.getRandChordByTonality(keynote.getIdKeynote(), typeChordIds.toArray(new Integer[0]));
    }

}
