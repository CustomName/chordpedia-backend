package org.nosite.chordpedia.rest.open;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.nosite.chordpedia.model.GroupTypeChord;
import org.nosite.chordpedia.model.TypeChord;
import org.nosite.chordpedia.services.GroupTypeChordService;
import org.nosite.chordpedia.services.TypeChordService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api/public")
@Slf4j
@AllArgsConstructor
public class TypeChordController {

    private final TypeChordService typeChordService;
    private final GroupTypeChordService groupTypeChordService;

    @GetMapping("/type-chord/{code}")
    public TypeChord getTypeChordByCode(@PathVariable("code") String code){
        TypeChord typeChord = typeChordService.getTypeChordByCode(code);

        return typeChord;
    }

    @GetMapping("/type-chord-by-id/{idTypeChord}")
    public TypeChord getTypeChordByIdTypeChord(@PathVariable("idTypeChord") Long idTypeChord){
        TypeChord typeChord = typeChordService.getTypeChordByIdTypeChord(idTypeChord);

        return typeChord;
    }

    @GetMapping("/type-chords")
    public List<TypeChord> getTypeChords(){
        List<TypeChord> typeChords = typeChordService.getTypeChords();
        typeChords.sort(Comparator.comparing(TypeChord::getOrderTypeChord));

        return typeChords;
    }

    @GetMapping("/type-chords-grouped")
    public Map<String, List<TypeChord>> getTypeChordsGrouped(){
        Map<String, List<TypeChord>> typeChordsMap = new LinkedHashMap<>();
        List<GroupTypeChord> groupTypeChords = groupTypeChordService.getGroupTypeChords();
        List<TypeChord> typeChords = typeChordService.getTypeChords();

        groupTypeChords.sort(Comparator.comparing(GroupTypeChord::getOrderGroupTypeChord));
        typeChords.sort(Comparator.comparing(TypeChord::getOrderTypeChord));
        groupTypeChords.forEach(group -> typeChordsMap.put(group.getValue(), new LinkedList<>()));
        typeChords.forEach(typeChord -> typeChordsMap.get(typeChord.getGroupTypeChord().getValue()).add(typeChord));

        return typeChordsMap;
    }

}
