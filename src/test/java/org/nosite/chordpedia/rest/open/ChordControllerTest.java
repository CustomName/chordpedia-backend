package org.nosite.chordpedia.rest.open;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.SecurityConfig;
import org.nosite.chordpedia.model.Chord;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.model.TypeChord;
import org.nosite.chordpedia.rest.SecurityTestConfig;
import org.nosite.chordpedia.services.ChordService;
import org.nosite.chordpedia.services.TonalityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Random;

import static java.util.Collections.singletonList;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ChordController.class, excludeAutoConfiguration = SecurityConfig.class)
@Import(SecurityTestConfig.class)
class ChordControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ChordService chordService;

    @MockBean
    private TonalityService tonalityService;

    @MockBean
    private Random random;

    @Test
    void getChordsCount() throws Exception {
        given(chordService.getChordsCount()).willReturn(3L);

        mvc.perform(get("/api/public/chords/count"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect((jsonPath("$").value(3)));
    }

    @Test
    void getChord() throws Exception {
        Chord chord = Chord.builder()
                .idChord(1L)
                .isDifficult(true)
                .build();

        given(chordService.getChordById(chord.getIdChord())).willReturn(chord);

        mvc.perform(get("/api/public/chord/" + chord.getIdChord()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect((jsonPath("$.idChord").value(chord.getIdChord())))
                .andExpect((jsonPath("$.difficult").value(chord.isDifficult())));
    }

    @Test
    void getChordsByValue() throws Exception {
        final String keynoteCode = "C";
        final String typeChordCode = "major";
        final String chordValue = keynoteCode + typeChordCode;
        final Keynote keynote = Keynote.builder().diezCode(keynoteCode).build();
        final TypeChord typeChord = TypeChord.builder().codeView(typeChordCode).build();
        Chord chord = Chord.builder()
                .startFret((short) 1)
                .keynote(keynote)
                .typeChord(typeChord)
                .build();

        given(chordService.findAllByKeynoteAndTypeChord(chordValue)).willReturn(singletonList(chord));

        mvc.perform(get("/api/public/chords/" + chordValue))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect((jsonPath("$.[0].keynote.diezCode").value(keynoteCode)))
                .andExpect((jsonPath("$.[0].typeChord.codeView").value(typeChordCode)));
    }

}