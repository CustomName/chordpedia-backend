package org.nosite.chordpedia.rest.open;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.SecurityConfig;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.rest.SecurityTestConfig;
import org.nosite.chordpedia.services.KeynoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = KeynoteController.class, excludeAutoConfiguration = SecurityConfig.class)
@Import(SecurityTestConfig.class)
class KeynoteControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private KeynoteService keynoteService;

    @Test
    void getKeynoteByCode() throws Exception {
        final String code = "Eb";

        Keynote keynote = Keynote.builder()
                .code(code)
                .build();

        given(keynoteService.getKeynoteByCode(code)).willReturn(keynote);

        mvc.perform(get("/api/public/keynote/" + code))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect((jsonPath("$.code").value(code)));
    }

    @Test
    void getKeynoteByIdKeynote() throws Exception {
        final long keynoteId = 555L;

        Keynote keynote = Keynote.builder()
                .idKeynote(keynoteId)
                .build();

        given(keynoteService.getKeynoteByIdKeynote(keynoteId)).willReturn(keynote);

        mvc.perform(get("/api/public/keynote-by-id/" + keynoteId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect((jsonPath("$.idKeynote").value(keynoteId)));
    }

}