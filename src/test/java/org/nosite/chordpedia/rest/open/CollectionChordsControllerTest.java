package org.nosite.chordpedia.rest.open;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.SecurityConfig;
import org.nosite.chordpedia.converters.CollectionChordsConverter;
import org.nosite.chordpedia.dto.CollectionChordsDto;
import org.nosite.chordpedia.jwt.JwtProvider;
import org.nosite.chordpedia.model.CollectionChord;
import org.nosite.chordpedia.rest.SecurityTestConfig;
import org.nosite.chordpedia.services.CollectionChordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CollectionChordsController.class, excludeAutoConfiguration = SecurityConfig.class)
@Import(SecurityTestConfig.class)
class CollectionChordsControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private CollectionChordService collectionChordService;

    @MockBean
    private CollectionChordsConverter collectionChordsConverter;

    @MockBean
    private JwtProvider jwtProvider;

    @Test
    void getCollectionChordByUuid() throws Exception {
        final String uuid = "uuid777";

        CollectionChord collectionChord = CollectionChord.builder()
                .uuid(uuid)
                .isPublic(true)
                .build();

        CollectionChordsDto collectionChordsDto = CollectionChordsDto.builder()
                .uuid(collectionChord.getUuid())
                .isPublic(collectionChord.isPublic())
                .build();

        given(collectionChordService.getCollectionChordByUuid(uuid)).willReturn(collectionChord);
        given(collectionChordsConverter.convert(collectionChord)).willReturn(collectionChordsDto);

        mvc.perform(get("/api/public/collection/" + uuid))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect((jsonPath("$.uuid").value(uuid)))
            .andExpect((jsonPath("$.public").value(true)));
    }

    @Test
    void getCollectionChords() throws Exception {
        CollectionChord collectionChord = CollectionChord.builder()
                .build();

        CollectionChordsDto collectionChordsDto = CollectionChordsDto.builder()
                .build();

        given(collectionChordService.findCollectionChordsWithAdminAuthorWithMinData(2))
                .willReturn(Arrays.asList(collectionChord, collectionChord));
        given(collectionChordsConverter.convert(collectionChord)).willReturn(collectionChordsDto);

        mvc.perform(get("/api/public/collections").param("limit", "2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)));
    }

}