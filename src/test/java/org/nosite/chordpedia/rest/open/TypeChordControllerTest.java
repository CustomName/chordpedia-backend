package org.nosite.chordpedia.rest.open;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.SecurityConfig;
import org.nosite.chordpedia.model.TypeChord;
import org.nosite.chordpedia.rest.SecurityTestConfig;
import org.nosite.chordpedia.services.GroupTypeChordService;
import org.nosite.chordpedia.services.TypeChordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = TypeChordController.class, excludeAutoConfiguration = SecurityConfig.class)
@Import(SecurityTestConfig.class)
class TypeChordControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TypeChordService typeChordService;

    @MockBean
    private GroupTypeChordService groupTypeChordService;

    @Test
    void getTypeChordByCode() throws Exception {
        final String code = "power";

        TypeChord typeChord = TypeChord.builder()
                .code(code)
                .build();

        given(typeChordService.getTypeChordByCode(code)).willReturn(typeChord);

        mvc.perform(get("/api/public/type-chord/" + code))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect((jsonPath("$.code").value(code)));
    }

    @Test
    void getTypeChordByIdTypeChord() throws Exception {
        final long typeChordId = 567345L;

        TypeChord typeChord = TypeChord.builder()
                .idTypeChord(typeChordId)
                .build();

        given(typeChordService.getTypeChordByIdTypeChord(typeChordId)).willReturn(typeChord);

        mvc.perform(get("/api/public/type-chord-by-id/" + typeChordId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect((jsonPath("$.idTypeChord").value(typeChordId)));
    }

    @Test
    void getTypeChords() throws Exception {
        given(typeChordService.getTypeChords()).willReturn(Arrays.asList(
                TypeChord.builder().build(),
                TypeChord.builder().build()
        ));

        mvc.perform(get("/api/public/type-chords"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect((jsonPath("$", hasSize(2))));
    }

}