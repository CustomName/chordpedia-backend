package org.nosite.chordpedia.rest.open;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.SecurityConfig;
import org.nosite.chordpedia.model.Tonality;
import org.nosite.chordpedia.rest.SecurityTestConfig;
import org.nosite.chordpedia.services.TonalityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = TonalityController.class, excludeAutoConfiguration = SecurityConfig.class)
@Import(SecurityTestConfig.class)
class TonalityControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TonalityService tonalityService;

    @Test
    void getTonalityByIdTonality() throws Exception {
        final long tonalityId = 43345L;

        Tonality tonality = Tonality.builder()
                .idTonality(tonalityId)
                .build();

        given(tonalityService.getTonalityById(tonalityId)).willReturn(tonality);

        mvc.perform(get("/api/public/tonality-by-id/" + tonalityId))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect((jsonPath("$.idTonality").value(tonalityId)));
    }

    @Test
    void getTonalities() throws Exception {
        given(tonalityService.getTonalities()).willReturn(Arrays.asList(
                Tonality.builder().orderTonality(1).build(),
                Tonality.builder().orderTonality(2).build()
        ));

        mvc.perform(get("/api/public/tonalities"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect((jsonPath("$", hasSize(2))));
    }

}