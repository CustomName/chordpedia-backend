package org.nosite.chordpedia.repos;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.AppConfig;
import org.nosite.chordpedia.model.CollectionChord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(AppConfig.class)
@TestPropertySource(locations = "classpath:application-test.yml")
class CollectionChordRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CollectionChordRepository collectionChordRepository;

    @Test
    void findByUuid() {
        final String uuid = "FAKEUUID";

        CollectionChord collectionChord = CollectionChord.builder()
                .uuid(uuid)
                .build();

        entityManager.persist(collectionChord);
        entityManager.flush();

        Optional<CollectionChord> actCollectionChord = collectionChordRepository.findByUuid(uuid);

        assertThat(actCollectionChord.isPresent()).isTrue();
        assertThat(actCollectionChord.get().getUuid()).isEqualTo(uuid);
    }

    @Test
    void findCollectionChordsWithAdminAuthor() {
        addCollectionChord(true, 3);
        addCollectionChord(false, 1);

        List<CollectionChord> actCollectionChords = collectionChordRepository.findCollectionChordsWithAdminAuthor();

        assertThat(actCollectionChords.size()).isEqualTo(3);
    }

    @Test
    void findCollectionChordsWithAdminAuthorWithLimit() {
        addCollectionChord(true, 3);

        List<CollectionChord> actCollectionChords = collectionChordRepository.findCollectionChordsWithAdminAuthorWithLimit(2);

        assertThat(actCollectionChords.size()).isEqualTo(2);
    }

    private void addCollectionChord(boolean isAdminAuthor, int size){
        for(int i = 0; i < size; i++){
            entityManager.persist(CollectionChord.builder()
                    .isAdminAuthor(isAdminAuthor)
                    .build());
            entityManager.flush();
        }
    }

}