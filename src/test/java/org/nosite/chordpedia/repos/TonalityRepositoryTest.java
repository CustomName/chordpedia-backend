package org.nosite.chordpedia.repos;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.AppConfig;
import org.nosite.chordpedia.model.Tonality;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(AppConfig.class)
@TestPropertySource(locations = "classpath:application-test.yml")
class TonalityRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TonalityRepository tonalityRepository;

    @Test
    void findRandTonality() {
        final String tonalityName = "CMajor";

        Tonality tonality = Tonality.builder()
                .name(tonalityName)
                .build();
        entityManager.persist(tonality);
        entityManager.flush();

        Optional<Tonality> actTonality = tonalityRepository.findRandTonality();

        assertThat(actTonality.isPresent()).isTrue();
        assertThat(actTonality.get().getName()).isEqualTo(tonalityName);
    }

}