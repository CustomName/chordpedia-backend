package org.nosite.chordpedia.repos;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.AppConfig;
import org.nosite.chordpedia.model.Chord;
import org.nosite.chordpedia.model.Keynote;
import org.nosite.chordpedia.model.TypeChord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(AppConfig.class)
@TestPropertySource(locations = "classpath:application-test.yml")
class ChordRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ChordRepository chordRepository;

    @Test
    void findAllByKeynoteAndTypeChord() {
        final String keynoteCode = "C";
        final String typeChordCode = "major";
        final Keynote keynote = Keynote.builder().diezCode(keynoteCode).build();
        final TypeChord typeChord = TypeChord.builder().codeView(typeChordCode).build();

        entityManager.persist(keynote);
        entityManager.persist(typeChord);
        entityManager.persist(Chord.builder()
                .keynote(keynote)
                .typeChord(typeChord)
                .build());
        entityManager.flush();

        List<Chord> actChords = chordRepository.findAllByKeynoteAndTypeChord(keynoteCode, typeChordCode);

        assertThat(actChords.size()).isEqualTo(1);
    }

    @Test
    void findRandChord() {
        final short startFret = 12;
        Chord chord = entityManager.persist(Chord.builder().startFret(startFret).build());

        entityManager.flush();

        Optional<Chord> actChord = chordRepository.findRandChord();

        assertThat(actChord.isPresent()).isTrue();
    }

}