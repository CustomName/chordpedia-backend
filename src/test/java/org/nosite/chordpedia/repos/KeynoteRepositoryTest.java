package org.nosite.chordpedia.repos;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.AppConfig;
import org.nosite.chordpedia.model.Keynote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(AppConfig.class)
@TestPropertySource(locations = "classpath:application-test.yml")
class KeynoteRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private KeynoteRepository keynoteRepository;

    @Test
    void findByCode() {
        final String code = "C";

        Keynote keynote = Keynote.builder()
                .code(code)
                .build();
        entityManager.persist(keynote);
        entityManager.flush();

        Optional<Keynote> actKeynote = keynoteRepository.findByCode(code);

        assertThat(actKeynote.isPresent()).isTrue();
        assertThat(actKeynote.get().getCode()).isEqualTo(code);
    }

}