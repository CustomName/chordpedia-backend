package org.nosite.chordpedia.repos;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.AppConfig;
import org.nosite.chordpedia.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(AppConfig.class)
@TestPropertySource(locations = "classpath:application-test.yml")
class AppUserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AppUserRepository appUserRepository;

    @Test
    void findByLogin() {
        final String login = "LOGIN";

        AppUser appUser = new  AppUser();
        appUser.setLogin(login);

        entityManager.persist(appUser);
        entityManager.flush();

        Optional<AppUser> actAppUser = appUserRepository.findByLogin(login);

        assertThat(actAppUser.isPresent()).isTrue();
        assertThat(actAppUser.get().getLogin()).isEqualTo(login);
    }

}