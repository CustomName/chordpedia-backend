package org.nosite.chordpedia.repos;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.nosite.chordpedia.configurations.AppConfig;
import org.nosite.chordpedia.model.TypeChord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(AppConfig.class)
@TestPropertySource(locations = "classpath:application-test.yml")
class TypeChordRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TypeChordRepository typeChordRepository;

    @Test
    void findByCode() {
        final String code = "Major";

        TypeChord typeChord = TypeChord.builder()
                .code(code)
                .build();

        entityManager.persist(typeChord);
        entityManager.flush();

        Optional<TypeChord> actTypeChord = typeChordRepository.findByCode(code);

        assertThat(actTypeChord.isPresent()).isTrue();
        assertThat(actTypeChord.get().getCode()).isEqualTo(code);
    }

}