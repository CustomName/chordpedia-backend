## Сайт

Проект запущен в эксплуатацию на [chordpedia.org](https://chordpedia.org)

## Front-end

Доступен по ссылке [chordpedia-frontend](https://gitlab.com/CustomName/chordpedia-frontend)

## Distributed Configuration

Секреты хранятся в консуле:
- config/chordpedia/secrets/key/store/password
- config/chordpedia/secrets/spring/datasource/password
- config/chordpedia/secrets/spring/datasource/username
- config/chordpedia/secrets/jwt/secret
